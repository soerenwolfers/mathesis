# README #

### What is this repository for? ###

* This repository contains a LaTeX package that summarizes useful packages that are frequently used for the creation of mathematical articles and theses
* Version 1.0

### How do I get set up? ###

* Download file `mathesis.sty` into your project directory
* Put the command `\usepackage{mathesis}` in your preamble
* Check `template/main.tex` for documentation and example usage of the package
* If you do not want to copy `mathesis.sty` every time you create a new article, you may copy it into `<TEXMF>/tex/latex/mathesis`. The value of `<TEXMF>` depends on your TeX distribution and your operating system, check out http://tex.stackexchange.com/questions/1137/where-do-i-place-my-own-sty-or-cls-files-to-make-them-available-to-all-my-te

### Who do I talk to? ###

* soeren.wolfers@gmail.com
